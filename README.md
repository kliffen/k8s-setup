# Setting up a Kubernetes cluster

This repository describes how to setup a Kubernetes (k8s) cluster
and how to deploy services on it.

## Steps

### VM
Create/have access to a VM and install an OS to it (using Debian 11 in this guide)

#### Unique VM's
Make sure each VM has it's own MAC address.

I used these with Virtual Box (and an example reserved ip)

08:00:27:AA:AA:AA | 192.168.1.100
08:00:27:BB:BB:BB | 192.168.1.101
08:00:27:CC:CC:CC | 192.168.1.102
08:00:27:DD:DD:DD | 192.168.1.103

### Disable Swap
Disable swap (if present):
```bash
# Verify if current swap status
free -h
# disables swap on the running system
swapoff -a
# Verify swap is at 0 bytes
free -h
# Then comment out the line for swap in /etc/fstab
nano /etc/fstab
# The line looks like:
#UUID=<uuid>    none    swap    sw  0   0
```

### Prerequisites

For Debian 11:
```bash
apt-get update
apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg \
    lsb-release

mkdir -p /etc/apt/keyrings  # new in Debian 12/Ubuntu 22.04
```

### TODO:

#### Forwarding IPv4 and letting iptables see bridged traffic

Execute the below mentioned instructions:
```bash
cat <<EOF | sudo tee /etc/modules-load.d/k8s.conf
overlay
br_netfilter
EOF

sudo modprobe overlay
sudo modprobe br_netfilter

# sysctl params required by setup, params persist across reboots

cat <<EOF | sudo tee /etc/sysctl.d/k8s.conf
net.bridge.bridge-nf-call-iptables  = 1
net.bridge.bridge-nf-call-ip6tables = 1
net.ipv4.ip_forward                 = 1
EOF

# Apply sysctl params without reboot
sudo sysctl --system
```

Verify that the br_netfilter, overlay modules are loaded by running below instructions:

```bash
lsmod | grep br_netfilter
lsmod | grep overlay
```

Verify that the net.bridge.bridge-nf-call-iptables, net.bridge.bridge-nf-call-ip6tables, net.ipv4.ip_forward system variables are set to 1 in your sysctl config by running below instruction:

```bash
sysctl net.bridge.bridge-nf-call-iptables net.bridge.bridge-nf-call-ip6tables net.ipv4.ip_forward
```

Check [this](https://kubernetes.io/docs/setup/production-environment/container-runtimes/#containerd) too.

### Install Containerd

Docker is not officially supported, but `containerd` is. Docker is just the "front-end" of `containerd`.

Remove old versions of Docker:
```bash
apt-get remove docker docker-engine docker.io containerd runc
```

Add the docker key and repository (assuming you are root):
```
curl -fsSL https://download.docker.com/linux/debian/gpg | gpg --dearmor -o /etc/apt/keyrings/docker.gpg
echo \
    "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/debian \
    $(lsb_release -cs) stable" | tee /etc/apt/sources.list.d/docker.list > /dev/null

apt-get update
apt-get install containerd.io
```

### Install Kubernetes

Assuming you are root:
```bash
# Add the repository
curl -fsSLo /etc/apt/keyrings/kubernetes-archive-keyring.gpg https://packages.cloud.google.com/apt/doc/apt-key.gpg
echo "deb [signed-by=/etc/apt/keyrings/kubernetes-archive-keyring.gpg] https://apt.kubernetes.io/ kubernetes-xenial main" | tee /etc/apt/sources.list.d/kubernetes.list

apt-get update
# Install the necessary tools:
apt-get install -y kubelet kubeadm kubectl
apt-mark hold kubelet kubeadm kubectl   # note the hold. Upgrades are not automatic!
```

The `kubelet` service is running (but failing, since it is not configured):
```bash
systemctl status kubelet
```
